import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _ce11d332 = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _1c4f1c76 = () => interopDefault(import('../pages/auto.vue' /* webpackChunkName: "pages/auto" */))
const _faa5fa3c = () => interopDefault(import('../pages/bio.vue' /* webpackChunkName: "pages/bio" */))
const _e550f642 = () => interopDefault(import('../pages/biogeo.vue' /* webpackChunkName: "pages/biogeo" */))
const _fac87040 = () => interopDefault(import('../pages/caco.vue' /* webpackChunkName: "pages/caco" */))
const _1353a9ae = () => interopDefault(import('../pages/carto.vue' /* webpackChunkName: "pages/carto" */))
const _334112c8 = () => interopDefault(import('../pages/choreo.vue' /* webpackChunkName: "pages/choreo" */))
const _86f53752 = () => interopDefault(import('../pages/crypto.vue' /* webpackChunkName: "pages/crypto" */))
const _61b3500e = () => interopDefault(import('../pages/demo.vue' /* webpackChunkName: "pages/demo" */))
const _ae54e176 = () => interopDefault(import('../pages/filmo.vue' /* webpackChunkName: "pages/filmo" */))
const _f74e6eaa = () => interopDefault(import('../pages/geo.vue' /* webpackChunkName: "pages/geo" */))
const _d8018fca = () => interopDefault(import('../pages/ideo.vue' /* webpackChunkName: "pages/ideo" */))
const _571ce5a8 = () => interopDefault(import('../pages/lexico.vue' /* webpackChunkName: "pages/lexico" */))
const _591df5ae = () => interopDefault(import('../pages/mytho.vue' /* webpackChunkName: "pages/mytho" */))
const _26d1b95f = () => interopDefault(import('../pages/paleo.vue' /* webpackChunkName: "pages/paleo" */))
const _b9488742 = () => interopDefault(import('../pages/picto.vue' /* webpackChunkName: "pages/picto" */))
const _1f28ac00 = () => interopDefault(import('../pages/semiporno.vue' /* webpackChunkName: "pages/semiporno" */))
const _8ea21da8 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/elemental-ytterbium/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _ce11d332,
    name: "about"
  }, {
    path: "/auto",
    component: _1c4f1c76,
    name: "auto"
  }, {
    path: "/bio",
    component: _faa5fa3c,
    name: "bio"
  }, {
    path: "/biogeo",
    component: _e550f642,
    name: "biogeo"
  }, {
    path: "/caco",
    component: _fac87040,
    name: "caco"
  }, {
    path: "/carto",
    component: _1353a9ae,
    name: "carto"
  }, {
    path: "/choreo",
    component: _334112c8,
    name: "choreo"
  }, {
    path: "/crypto",
    component: _86f53752,
    name: "crypto"
  }, {
    path: "/demo",
    component: _61b3500e,
    name: "demo"
  }, {
    path: "/filmo",
    component: _ae54e176,
    name: "filmo"
  }, {
    path: "/geo",
    component: _f74e6eaa,
    name: "geo"
  }, {
    path: "/ideo",
    component: _d8018fca,
    name: "ideo"
  }, {
    path: "/lexico",
    component: _571ce5a8,
    name: "lexico"
  }, {
    path: "/mytho",
    component: _591df5ae,
    name: "mytho"
  }, {
    path: "/paleo",
    component: _26d1b95f,
    name: "paleo"
  }, {
    path: "/picto",
    component: _b9488742,
    name: "picto"
  }, {
    path: "/semiporno",
    component: _1f28ac00,
    name: "semiporno"
  }, {
    path: "/",
    component: _8ea21da8,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
