const pkg = require('./package')

module.exports = {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    script: [
      // { src: 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js' },
      { src: 'node_modules/jquery/dist/jquery.js' },
      { src: 'node_modules/uikit/dist/js/uikit.js' },
      // { src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js' },
      // { src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.1/js/uikit-icons.min.js' },
      // { src: 'https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit-icons.min.js' }
      { src: 'node_modules/uikit/dist/js/uikit-icons.min.js' }
    ],
    title: "Elemental Ytterbium Quibbles",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel: 'stylesheet', type: 'text/csss', href: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.1/css/uikit.min.css'},
      { rel: 'stylesheet', type: 'text/csss', href: 'https://fonts.googleapis.com/css?family=News+Cycle|Material+Icons' },
      { rel: 'stylesheet', type: 'text/csss', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css' },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
      // '@/assets/scss/main.css'
      './assets/scss/custom.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/uikit.js', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // '@nuxtjs/vuetify',
  ],

  /*
  ** Build configuration
  */
  build: {
      /*
      ** You can extend webpack config here
      */
      extend(config, ctx) {
    }
  },
  router: {
      base: '/elemental-ytterbium',
  },
  generate: {
    dir: 'public',
  }
}